﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Media;
using InterfaceBuilder.Models.Base;
using InterfaceBuilder.Models.UserElements;

namespace InterfaceBuilder.Models
{
    public class ApplicationViewModel : ControlBase
    {
        public ObservableCollection<UserElementBase> Controls { get; }

        public ApplicationViewModel()
        {
            Width = 800;
            Height = 600;
            BackgroundBrush = Brushes.Beige;
            Controls = new ObservableCollection<UserElementBase>();
        }

        public void AddControl(InterfaceModel model, int top, int left)
        {
            var control = Activator.CreateInstance(model.ControlType) as UserElementBase;
            if (control == null) throw new Exception("Wrong type");
            control.Top = top;
            control.Left = left;
            control.ParentViewModel = this;

            Controls.Add(control);
        }

        #region Commands


        #endregion Commands
    }
}