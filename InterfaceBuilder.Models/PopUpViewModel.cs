﻿namespace InterfaceBuilder.MVVM
{
    public class PopUpViewModel : ViewModelBase
    {
        public double Width { get; set; }
        public double Height { get; set; }
    }
}