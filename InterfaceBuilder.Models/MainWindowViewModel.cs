﻿using System.Windows;
using System.Windows.Input;
using InterfaceBuilder.MVVM;

namespace InterfaceBuilder.Models
{
    public class MainWindowViewModel : ViewModelBase
    {
        public MainWindowViewModel()
        {
            WindowState = WindowState.Normal;
        }

        #region Properties

        private WindowState m_state;
        public WindowState WindowState
        {
            get => m_state;
            set
            {
                m_state = value;
                RaisePropertyChanged();
            }
        }

        #endregion Properties

        #region Commands

        public ICommand MinimizeCommand => new BuilderCommand(() => WindowState = WindowState.Minimized);
        public ICommand MaximizeCommand => new BuilderCommand(() => WindowState = WindowState.Maximized);
        public ICommand CloseCommand => new BuilderCommand(() => Application.Current.Shutdown(1));

        #endregion Commands
    }
}