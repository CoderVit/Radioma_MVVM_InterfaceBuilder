﻿using System.Collections.ObjectModel;
using InterfaceBuilder.Models.UserElements;
using InterfaceBuilder.MVVM;

namespace InterfaceBuilder.Models
{
    public class InterfaceControlsViewModel : ViewModelBase
    {
        public InterfaceControlsViewModel()
        {
            InterfaceControlBases = new ObservableCollection<InterfaceModel>
            {
                new InterfaceModel
                {
                    Name = "Контейнер",
                    ControlType = typeof(ChannelViewModel)
                },
                new InterfaceModel
                {
                    Name = "Индикатор",
                    ControlType = typeof(IndicatorViewModel)
                },
                new InterfaceModel
                {
                    Name = "Описание",
                    ControlType = typeof(TextViewModel)
                },
                new InterfaceModel
                {
                    Name = "Кнопка",
                    ControlType = typeof(ButtonViewModel)
                },
                new InterfaceModel
                {
                    Name = "Уровень",
                    ControlType = typeof(LevelViewModel)
                },
            };
        }

        #region Properties

        public ObservableCollection<InterfaceModel> InterfaceControlBases { get; }

        #endregion Properties
    }
}