﻿using System.Windows.Media;

namespace InterfaceBuilder.Models.UserElements
{
    public class TextViewModel : UserElementBase
    {
        public TextViewModel()
        {
            MinHeight = 10;
            Height = 20;
            MaxHeight = 40;
            MinWidth = 40;
            Width = 80;
            MaxWidth = 120;
            ZIndex = 2;
            BackgroundBrush = Brushes.DarkBlue;
            Text = "Описание";
        }

        #region Properties

        private string m_text;
        public string Text
        {
            get => m_text;
            set
            {
                if (Equals(m_text == value)) return;
                m_text = value;
                RaisePropertyChanged();
            }
        }

        #endregion Properties
    }
}