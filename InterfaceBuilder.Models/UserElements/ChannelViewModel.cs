﻿using System;
using System.Windows.Media;

namespace InterfaceBuilder.Models.UserElements
{
    public class ChannelViewModel : UserElementBase
    {
        public ChannelViewModel()
        {
            Guid = new Guid();
            MinHeight = 200;
            Height = 400;
            MaxHeight = 800;
            MinWidth = 100;
            Width = 200;
            MaxWidth = 400;
            ZIndex = 1;
            BackgroundBrush = Brushes.Bisque;
        }

        #region Properties
        public Guid Guid { get; }

        #endregion Properties
    }
}