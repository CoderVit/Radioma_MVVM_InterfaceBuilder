﻿using InterfaceBuilder.Models.Base;

namespace InterfaceBuilder.Models.UserElements
{
    public class UserElementBase : ControlBase
    {
        #region Properties

        public ControlBase ParentViewModel { get; internal set; }

        //Отступ от верха канвы
        private double m_top;
        public double Top
        {
            get => m_top;
            set
            {
                if (Equals(m_top, value)) return;
                m_top = value;
                RaisePropertyChanged();
            }
        }

        //Отступ слева канвы
        private double m_left;
        public double Left
        {
            get => m_left;
            set
            {
                if (Equals(m_left, value)) return;
                m_left = value;
                RaisePropertyChanged();
            }
        }

        //приоритет отображения свойства
        public byte ZIndex { get; internal set; }

        #endregion Properties
    }
}