﻿using System.Windows.Media;


namespace InterfaceBuilder.Models.UserElements
{
    public class ButtonViewModel : UserElementBase
    {
        public ButtonViewModel()
        {
            MinHeight = 35;
            Height = 70;
            MaxHeight = 150;
            MinWidth = 75;
            Width = 150;
            MaxWidth = 300;
            ZIndex = 2;
            BackgroundBrush = Brushes.DarkOliveGreen;
            Content = "Click me";
        }

        #region Properties

        private string m_content;
        public string Content
        {
            get => m_content;
            set
            {
                if (Equals(m_content == value)) return;
                m_content = value;
                RaisePropertyChanged();
            }
        }

        #endregion Properties
    }
}