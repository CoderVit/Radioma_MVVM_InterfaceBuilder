﻿using System.Windows.Media;

namespace InterfaceBuilder.Models.UserElements
{
    public class LevelViewModel : UserElementBase
    {
        public LevelViewModel()
        {
            MinHeight = 50;
            Height = 100;
            MaxHeight = 200;
            MinWidth = 25;
            Width = 50;
            MaxWidth = 100;
            ZIndex = 2;
            BackgroundBrush = Brushes.ForestGreen;
        }

        #region Properties


        #endregion Properties
    }
}
