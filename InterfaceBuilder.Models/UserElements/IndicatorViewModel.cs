﻿using System.Windows.Media;

namespace InterfaceBuilder.Models.UserElements
{
    public class IndicatorViewModel : UserElementBase
    {
        public IndicatorViewModel()
        {
            MinHeight = MinWidth = 10;
            Height = Width = 40;
            ZIndex = 2;
            BackgroundBrush = Brushes.Red;
        }

        #region Properties

        //props

        #endregion Properties
    }
}