﻿using System.Windows.Media;
using InterfaceBuilder.MVVM;

namespace InterfaceBuilder.Models.Base
{
    public class ControlBase : ViewModelBase
    {
        #region Properties

        public double MinWidth { get; internal set; }
        //Ширина контрола
        private double m_width;
        public double Width
        {
            get => m_width;
            set
            {
                if (Equals(m_width, value)) return;
                m_width = value;
                RaisePropertyChanged();
            }
        }
        public double MaxWidth { get; internal set; }

        public double MinHeight { get; internal set; }
        //Высота контрола
        private double m_height;
        public double Height
        {
            get => m_height;
            set
            {
                if (Equals(m_height, value)) return;
                m_height = value;
                RaisePropertyChanged();
            }
        }
        public double MaxHeight { get; internal set; }

        //задний фон контрола
        private Brush m_backgroundBrush;
        public Brush BackgroundBrush
        {
            get => m_backgroundBrush;
            set
            {
                if (Equals(m_backgroundBrush, value)) return;
                m_backgroundBrush = value;
                RaisePropertyChanged();
            }
        }

        #endregion
    }
}
