﻿using System.Windows.Input;
using InterfaceBuilder.MVVM;

namespace InterfaceBuilder.Models
{
    public class MenuViewModel : ViewModelBase
    {

        #region Properties



        #endregion Properties

        #region Commands

        public ICommand SaveAsCommand { get { return new BuilderCommand(() => { }); } }
        public ICommand SaveCommand { get { return new BuilderCommand(() => { }); } }
        public ICommand ExitCommand { get { return new BuilderCommand(() => { }); } }

        #endregion Commands


        //#region Main Context Menu

        //private void MenuItem_Save_OnClick(object sender, RoutedEventArgs e)
        //{
        //    //m_manager.Save(Channels.ToList());
        //}

        //private void MenuItem_Save_As_OnClick(object sender, RoutedEventArgs e)
        //{
        //    //m_manager.Save(Channels.ToList());
        //}

        //private void MenuItem_Exit_App_OnClick(object sender, RoutedEventArgs e)
        //{
        //    Close();
        //}

        //#endregion Main Context Menu
    }
}
