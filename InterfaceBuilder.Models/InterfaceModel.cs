﻿using System;

namespace InterfaceBuilder.Models
{
    public class InterfaceModel
    {
        public string Name { get; set; }
        public Type ControlType { get; set; }
    }
}
