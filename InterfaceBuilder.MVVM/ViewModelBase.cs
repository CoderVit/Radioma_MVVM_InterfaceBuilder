﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace InterfaceBuilder.MVVM
{
    /// <summary>
    /// базовый класс для моделей представления.
    /// реализующий интерфейс INotifyPropertyChanged
    /// </summary>
    public class ViewModelBase : INotifyPropertyChanged
    {
        #region MVVM related

        protected void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged == null || string.IsNullOrEmpty(propertyName)) return;
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion MVVM related
    }
}