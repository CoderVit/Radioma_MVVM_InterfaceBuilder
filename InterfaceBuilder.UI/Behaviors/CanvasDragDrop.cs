﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using InterfaceBuilder.Models;

namespace InterfaceBuilder.UI.Behaviors
{
    //добавления элемента с листбокса к модели представления с рендеренгом на канвас
    internal class CanvasDragDrop : Behavior<FrameworkElement>
    {
        private const string DATA_KEY = "InterfaceControl";

        protected override void OnAttached()
        {
            AssociatedObject.DragEnter += OnDragEnter;
            AssociatedObject.Drop += OnDrop;

            base.OnAttached();
        }

        protected override void OnDetaching()
        {
            AssociatedObject.DragEnter -= OnDragEnter;
            AssociatedObject.Drop -= OnDrop;

            base.OnDetaching();
        }

        private void OnDragEnter(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(DATA_KEY) || sender == e.Source)
            {
                e.Effects = DragDropEffects.None;
            }
        }

        private void OnDrop(object sender, DragEventArgs e)
        {
            var control = e.Data.GetData(DATA_KEY) as InterfaceModel;
            if (control is null || sender is null) return;
            var point = e.GetPosition((Canvas)sender);
            (AssociatedObject.DataContext as ApplicationViewModel)?.AddControl(control, RoundByDot(point.Y), RoundByDot(point.X));
        }

        private int RoundByDot(double coordinate)
        {
            return (int)(Math.Round(coordinate / 10) * 10);
        }
    }
}