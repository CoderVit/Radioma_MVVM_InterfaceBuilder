﻿//using System.Windows;
//using System.Windows.Controls;
//using System.Windows.Input;
//using System.Windows.Interactivity;
//using System.Windows.Media;

//namespace InterfaceBuilder.UI.Behaviors
//{
//    //перетаскивания элемента по канвасу
//    internal class ControlDrag : Behavior<UIElement>
//    {
//        private Point m_anchorPoint;

//        private double m_xLeft;
//        private double m_xRight;

//        private double m_yTop;
//        private double m_yBottom;

//        private double m_containerHeight;
//        private double m_containerWidth;

//        private readonly TranslateTransform m_transform = new TranslateTransform();

//        protected override void OnAttached()
//        {
//            AssociatedObject.RenderTransform = m_transform;

//            AssociatedObject.MouseLeftButtonDown += OnMouseLeftButtonDown;
//            AssociatedObject.MouseMove += OnMouseMove;
//            AssociatedObject.MouseLeftButtonUp += OnMouseLeftButtonUp;

//            base.OnAttached();
//        }

//        protected override void OnDetaching()
//        {
//            AssociatedObject.MouseLeftButtonDown -= OnMouseLeftButtonDown;
//            AssociatedObject.MouseMove -= OnMouseMove;
//            AssociatedObject.MouseLeftButtonUp -= OnMouseLeftButtonUp;
//            AssociatedObject.RenderTransform = null;

//            base.OnDetaching();
//        }

//        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
//        {
//            var t = e.GetPosition(AssociatedObject);
//            var element = AssociatedObject as UserControl;

//            m_xLeft = t.X;
//            m_xRight = element.ActualWidth - m_xLeft;


//            m_yTop = t.Y;
//            m_yBottom = element.ActualHeight - m_yTop;

//            m_containerHeight = Application.Current.MainWindow.ActualHeight;
//            m_containerWidth = Application.Current.MainWindow.ActualWidth;
//            m_anchorPoint = e.GetPosition(null);

//            AssociatedObject.CaptureMouse();
//        }

//        private void OnMouseMove(object sender, MouseEventArgs e)
//        {
//            if (!AssociatedObject.IsMouseCaptured) return;
//            var currentPoint = e.GetPosition(null);

//            if (currentPoint.X - m_xLeft < 0                                          //element position outside on left
//                || currentPoint.X + m_xRight > m_containerWidth                       //element position outside on right
//                || currentPoint.Y - m_yTop < 0                                        //element position outside on top
//                || currentPoint.Y + m_yBottom > m_containerHeight) return;            //element position outside on bottom

//            m_transform.X += currentPoint.X - m_anchorPoint.X;
//            m_transform.Y += currentPoint.Y - m_anchorPoint.Y;

//            m_anchorPoint = currentPoint;
//        }

//        private void OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
//        {
//            AssociatedObject.ReleaseMouseCapture();
//        }
//    }
//}