﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace InterfaceBuilder.UI.Behaviors
{
    //перетаскивание копии элемента с листбокса на канвас
    internal class ListBoxItemDrag : Behavior<FrameworkElement>
    {
        private const string DATA_KEY = "InterfaceControl";
        private Point m_startPoint;

        protected override void OnAttached()
        {
            AssociatedObject.PreviewMouseLeftButtonDown += OnPreviewMouseLeftButtonDown;
            AssociatedObject.MouseMove += OnMouseMove;
        }

        protected override void OnDetaching()
        {
            AssociatedObject.PreviewMouseLeftButtonDown -= OnPreviewMouseLeftButtonDown;
            AssociatedObject.MouseMove -= OnMouseMove;

            base.OnDetaching();
        }

        private void OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            m_startPoint = e.GetPosition(null);
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var diff = m_startPoint - e.GetPosition(null);

            if (e.LeftButton != MouseButtonState.Pressed ||
                !(Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance) &&
                !(Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance)) return;

            // Get the dragged ListBoxItem
            var listBox = sender as ListBox;
            if (listBox is null || listBox.SelectedItem is null) return;

            //Initialize the drag & drop operation
            var dragData = new DataObject(DATA_KEY, listBox.SelectedItem);
            DragDrop.DoDragDrop(listBox, dragData, DragDropEffects.Move);
        }
    }
}