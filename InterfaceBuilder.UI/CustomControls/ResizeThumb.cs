﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls.Primitives;
using InterfaceBuilder.Models.UserElements;

namespace InterfaceBuilder.UI.CustomControls
{
    internal class ResizeThumb : Thumb
    {
        private const byte RESIZE_STEP = 10;
        private UserElementBase m_elementContext;

        #region Constructor/Destructor

        public ResizeThumb()
        {
            Loaded += OnLoaded;
            DragDelta += OnDragDelta;
        }
        ~ResizeThumb()
        {
            DragDelta -= OnDragDelta;
            Loaded -= OnLoaded;
        }
        #endregion Constructor/Destructor

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            m_elementContext = (sender as ResizeThumb)?.DataContext as UserElementBase;
        }

        private void OnDragDelta(object sender, DragDeltaEventArgs e)
        {
            var verticalChange = Math.Round(e.VerticalChange / RESIZE_STEP) * RESIZE_STEP;
            var horizontalChange = Math.Round(e.HorizontalChange / RESIZE_STEP) * RESIZE_STEP;

            //TODO refactor this algorhythm
            if (Math.Abs(verticalChange) > 0)
            {
                if (VerticalAlignment == VerticalAlignment.Bottom)
                {
                    //новый размер со сдвигом + RESIZE_STEP вниз
                    var newSizeWithDelta = m_elementContext.Height + verticalChange;
                    //размер более ли чем максимально допустимый
                    var newY = newSizeWithDelta >= m_elementContext.MaxHeight ? m_elementContext.MaxHeight : newSizeWithDelta;
                    //вылазит ли новый размер за пределы родителя
                    m_elementContext.Height = newY + m_elementContext.Top > m_elementContext.ParentViewModel.Height ? m_elementContext.Height : newY;
                }
                if (VerticalAlignment == VerticalAlignment.Top)
                {
                    //новый размер со сдвигом + RESIZE_STEP вниз
                    var newSizeWithDelta = m_elementContext.Height + verticalChange;
                    //размер более ли чем максимально допустимый
                    var newY = newSizeWithDelta >= m_elementContext.MinHeight ? m_elementContext.MinHeight : newSizeWithDelta;

                    m_elementContext.Top = verticalChange;
                    m_elementContext.Height = newY + m_elementContext.Top > m_elementContext.ParentViewModel.Height ? m_elementContext.Height : newY;
                }
            }

            if (Math.Abs(horizontalChange) > 0)
            {
                if (HorizontalAlignment == HorizontalAlignment.Right)
                {
                    //новый размер со сдвигом + RESIZE_STEP вниз
                    var newSizeHeightDelta = m_elementContext.Width + horizontalChange;
                    //размер более ли чем максимально допустимый
                    var newX = newSizeHeightDelta >= m_elementContext.MaxWidth ? m_elementContext.MaxWidth : newSizeHeightDelta;
                    //вылазит ли новый размер за пределы родителя
                    m_elementContext.Width = newX + m_elementContext.Left > m_elementContext.ParentViewModel.Width ? m_elementContext.Width : newX;
                }

                if (HorizontalAlignment == HorizontalAlignment.Left)
                {

                }
            }

            //switch (VerticalAlignment)
            //{
            //    //case VerticalAlignment.Bottom:
            //    //    var newY = m_elementContext.Height + verticalChange;
            //    //    m_elementContext.Height = newY > m_elementContext.MaxHeight ? m_elementContext.MaxHeight : newY;
            //    //    break;
            //    case VerticalAlignment.Top:

            //        m_elementContext.Top += verticalChange;
            //        m_elementContext.Height -= verticalChange;
            //        break;
            //}

            //if (HorizontalAlignment == HorizontalAlignment.Center || HorizontalAlignment == HorizontalAlignment.Stretch) return;

            //var newX = m_elementContext.Width + horizontalChange <= 0 ? 0 : 5;

            //switch (HorizontalAlignment)
            //{
            //    case HorizontalAlignment.Left:
            //        m_elementContext.Left = newX;
            //        m_elementContext.Width = newX;
            //        break;
            //    case HorizontalAlignment.Right:
            //        m_elementContext.Width = newX > m_elementContext.MaxWidth ? m_elementContext.MaxWidth : newX;
            //        break;
            //}

            e.Handled = true;
        }
    }
}