﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;
using InterfaceBuilder.Models.UserElements;

namespace InterfaceBuilder.UI.CustomControls
{
    internal class MoveThumb : Thumb
    {
        private const byte MOVABLE_STEP = 10;
        private UserElementBase m_elementContext;

        #region Constructor/Destructor
        public MoveThumb()
        {
            Loaded += OnLoaded;
            DragDelta += OnDragDelta;
        }
        ~MoveThumb()
        {
            DragDelta -= OnDragDelta;
            Loaded -= OnLoaded;
        }
        #endregion Constructor/Destructor

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            m_elementContext = (sender as MoveThumb)?.DataContext as UserElementBase;
        }

        private void OnDragDelta(object sender, DragDeltaEventArgs e)
        {
            //округляем координаты в шаге MOVABLE_STEP
            var newX = Math.Round((e.HorizontalChange + m_elementContext.Left) / MOVABLE_STEP) * MOVABLE_STEP;
            var newY = Math.Round((e.VerticalChange + m_elementContext.Top) / MOVABLE_STEP) * MOVABLE_STEP;
            //если точка меньше ноля то ноль если более чем за границами родителя значит равно длине/широте родителя иначе полученное значение
            m_elementContext.Left = newX < 0 ? 0 : newX + m_elementContext.Width > m_elementContext.ParentViewModel.Width ? m_elementContext.Left : newX;
            m_elementContext.Top = newY < 0 ? 0 : newY + m_elementContext.Height > m_elementContext.ParentViewModel.Height ? m_elementContext.Top : newY;

            e.Handled = true;
        }
    }
}