﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace InterfaceBuilder.UI.Controls
{
    public partial class PopUp
    {
        public PopUp()
        {
            InitializeComponent();
        }

        Point m_anchorPoint;
        Point m_currentPoint;
        bool m_isInDrag;
        private readonly TranslateTransform m_transform = new TranslateTransform();

        private void Channel_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            var element = sender as FrameworkElement;
            m_anchorPoint = e.GetPosition(null);
            element?.CaptureMouse();
            m_isInDrag = true;
            e.Handled = true;
        }

        private void Channel_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (!m_isInDrag) return;

            m_currentPoint = e.GetPosition(null);

            m_transform.X += m_currentPoint.X - m_anchorPoint.X;
            m_transform.Y += m_currentPoint.Y - m_anchorPoint.Y;
            RenderTransform = m_transform;
            m_anchorPoint = m_currentPoint;
        }

        private void Channel_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (!m_isInDrag) return;
            var element = sender as FrameworkElement;
            if (element == null) return;
            element.ReleaseMouseCapture();
            m_isInDrag = false;
            e.Handled = true;
        }
    }
}
